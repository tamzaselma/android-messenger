package android.mbds.tamzaliselma.tp1;

import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Crypto {
    String provider = "AndroidKeyStore";
    String transformation = "RSA/ECB/PKCS1Padding";
    KeyPair kp;
    String stringToCrypt;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public Crypto() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException, UnsupportedEncodingException {
        generateKey();
        //String s = "a decrypter coco !";
        //byte[] c = crypte(s);
        //decrypte(c);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void generateKey() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", provider);
        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder("toto", KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_ECB)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1);
        kpg.initialize(builder.build());
        kp = kpg.genKeyPair();
    }

    public PublicKey getPublicKey() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException, UnrecoverableEntryException {
        KeyStore keyStore = KeyStore.getInstance(provider);
        keyStore.load(null);
        KeyStore.Entry entry = keyStore.getEntry("toto", null);
        PublicKey publicKey = keyStore.getCertificate("toto").getPublicKey();
        return publicKey;
    }

    public PrivateKey getPrivatKey() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException, UnrecoverableEntryException {
        KeyStore keyStore = KeyStore.getInstance(provider);
        keyStore.load(null);
        KeyStore.Entry entry = keyStore.getEntry("toto", null);
        PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
        return privateKey;
    }

    public byte[] crypte(String str) throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(Cipher.ENCRYPT_MODE, kp.getPublic());
        byte[] encryptedBytes = cipher.doFinal(str.getBytes());

        //System.out.println("-----> Chiffré : " + new String(encryptedBytes));
        return encryptedBytes;
    }


    public String decrypte(byte[] str) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        byte[] encryptedBytes = str;
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(Cipher.DECRYPT_MODE, kp.getPrivate());
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
        String decrypted = new String(decryptedBytes);

        //System.out.println("-----> Déchiffré : "+ decrypted);
        return decrypted;
    }



}
