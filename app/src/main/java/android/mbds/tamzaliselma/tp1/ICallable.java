package android.mbds.tamzaliselma.tp1;

import android.support.v4.app.Fragment;

public interface ICallable {

     void transferData(User p);
     Database getDatabase();
     ContactHelper getContactHelper();
     void backToContacts();
     boolean isLand();
}
