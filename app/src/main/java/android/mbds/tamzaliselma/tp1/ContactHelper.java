package android.mbds.tamzaliselma.tp1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContactHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_USERS =
            "CREATE TABLE " + Database.UserUser.FeedUser.TABLE_USER + " (" +
                    Database.UserUser.FeedUser._ID + " INTEGER PRIMARY KEY," +
                    Database.UserUser.FeedUser.COLUMN_NAME_USERNAME + " TEXT," +
                    Database.UserUser.FeedUser.COLUMN_NAME_PASSWORD + " TEXT)";

    private static final String SQL_DELETE_USERS = "DROP TABLE IF EXISTS " + Database.UserUser.FeedUser.TABLE_USER;
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_USER = "UserDb.db";


    public ContactHelper(Context context){
        super(context, DATABASE_USER, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USERS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_USERS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


}
