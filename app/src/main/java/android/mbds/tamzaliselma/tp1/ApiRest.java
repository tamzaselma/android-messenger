package android.mbds.tamzaliselma.tp1;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class ApiRest {
    OkHttpClient client;
    final static String URL_BASE = "http://baobab.tokidev.fr/api/";

    public ApiRest(OkHttpClient client){
        this.client = client;
    }

    Call login(String json, Callback callback) {
        RequestBody body = RequestBody.create(MediaType.get("application/json; charset=utf-8"), json);
        Request request = new Request.Builder()
                .url(URL_BASE + "login")
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }


    Call createUser( String json, Callback callback) {
        RequestBody body = RequestBody.create(MediaType.get("application/json; charset=utf-8"), json);
        Request request = new Request.Builder()
                .url(URL_BASE + "createUser")
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    Call sendMessage(JSONObject json, Callback callback) throws JSONException {
        JSONObject RequestBodyJson = new JSONObject();
        RequestBodyJson.put("message", json.get("message"));
        RequestBodyJson.put("receiver",json.get("receiver"));
        RequestBody body = RequestBody.create(MediaType.get("application/json; charset=utf-8"), RequestBodyJson.toString());
        Request request = new Request.Builder()
                .url(URL_BASE + "sendMsg")
                .addHeader("Authorization","Bearer " + json.get("token"))
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    Call getUserMessages(String token, Callback callback){
        Request request = new Request.Builder()
                .url(URL_BASE + "fetchMessages")
                .addHeader("Authorization","Bearer " + token)
                .get()
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }


}
