package android.mbds.tamzaliselma.tp1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Database {

    static ContactHelper cth;
    UserInfo userInfo;
    JSONObject JsonMessages;
    private static Database INSTANCE;
    private Database(Context ctx) {
        cth = new ContactHelper(ctx);
    }

    public static Database getInstance(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = new Database(ctx);
        }
        return INSTANCE;
    }


    public static Database getINSTANCE() {
        return INSTANCE;
    }

    public void addUser(User user){
        SQLiteDatabase db = cth.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Database.UserUser.FeedUser.COLUMN_NAME_USERNAME, user.username);
        values.put(Database.UserUser.FeedUser.COLUMN_NAME_PASSWORD, user.password);
        long newRowId = db.insert(Database.UserUser.FeedUser.TABLE_USER, null, values);
    }


    public List<User> readUser() {
        SQLiteDatabase db = cth.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                UserUser.FeedUser.COLUMN_NAME_USERNAME,
                UserUser.FeedUser.COLUMN_NAME_PASSWORD,
        };

        String selection = "";
        String[] selectionArgs = null;

        String sortOrder = UserUser.FeedUser.COLUMN_NAME_USERNAME + " DESC";

        Cursor cursor = db.query(
                UserUser.FeedUser.TABLE_USER,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,              // don't group the rows
                null,               // don't filter by row groups
                sortOrder               // The sort order
        );

        List users = new ArrayList<User>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(UserUser.FeedUser._ID));
            String username = cursor.getString(cursor.getColumnIndex(UserUser.FeedUser.COLUMN_NAME_USERNAME));
            String password = cursor.getString(cursor.getColumnIndex(UserUser.FeedUser.COLUMN_NAME_PASSWORD));
            users.add(new User(username, password));
        }
        cursor.close();
        return users;
    }

    public JSONObject getMessagesServer() {
        return this.JsonMessages;
    }

    public void setMessagesServer(JSONObject ja) {
        this.JsonMessages = ja;
    }

    public final class UserUser{
        private UserUser() {}
        public class FeedUser implements BaseColumns
        {
            public static final String TABLE_USER = "User";
            public static final String COLUMN_NAME_USERNAME = "Username";
            public static final String COLUMN_NAME_PASSWORD = "Password";
        }
    }


    public static class UserInfo {
        JSONObject userInfoJson;

        UserInfo(JSONObject userInfoJson) {
            this.userInfoJson = userInfoJson;
        }

        public String get(String attribut) {
            try {
                String Jarray = userInfoJson.getString(attribut);
                return Jarray;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void setCurrentUserInfo(JSONObject userInfoJson) {
        this.userInfo = new UserInfo(userInfoJson);
    }

}
