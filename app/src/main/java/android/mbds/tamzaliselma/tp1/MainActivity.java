package android.mbds.tamzaliselma.tp1;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity implements ICallable {

    Contacts contacts;
    Messages messages;
    Intent intent;
    FrameLayout fl ;
    FrameLayout fl1 ;
    FrameLayout fl2 ;
    Boolean a = true;
    public static Database db;
    public static ContactHelper contactHelper;
    OkHttpClient client;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.client = new OkHttpClient();
        this.contactHelper = new ContactHelper(this);
        this.contactHelper.onDowngrade(this.contactHelper.getWritableDatabase(),this.contactHelper.DATABASE_VERSION, this.contactHelper.DATABASE_VERSION+1);
        this.db = Database.getInstance(this);
        Intent i = new Intent(this,Login.class);
        startActivity(i);

        User p1 = new User("elbabizator", "elbabizator");
        db.addUser(p1);
        User p2 = new User("tamzaselma", "Selma");
        db.addUser(p2);
        User p3 = new User("Zochowska", "Agnieszka");
        db.addUser(p3);
        User p4 = new User("Gfrey","gggg");
        db.addUser(p4);
        User p5 = new User("Hawawa","hawawa");
        db.addUser(p5);
        User p6 = new User("Ala","ala");
        db.addUser(p6);
        User p7 = new User("Issoufi","Adam");
        db.addUser(p7);
        User p8 = new User("Vouit","vouit");
        db.addUser(p8);
        User p9 = new User("Chouchou","Chouchou");
        db.addUser(p9);
        User p10 = new User("Lulu","lulu");
        db.addUser(p10);
        User p11 = new User("lala","lulu");
        db.addUser(p11);
        User p12 = new User("lolo","lulu");
        db.addUser(p12);

        contacts = new Contacts();
        messages = new Messages();
        setContentView(R.layout.mainactivity);
        fl = findViewById(R.id.frame);
        fl1 = findViewById(R.id.frame1);
        fl2 = findViewById(R.id.frame2);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(fl == null){
            ft.replace(fl1.getId(), contacts);
            ft.replace(fl2.getId(), messages);
            ft.commit();
        }
        else{
            ft.replace(fl.getId(), contacts);
            ft.commit();
        }
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null){
            if ("text/plain".equals(type)) {
                handleSendText(intent);
            }
        }
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(fl.getId(), contacts);
            ft.commit();
            this.intent = intent;
        }
    }

    public void transferData(User p){
        if(intent != null){
            if(fl != null ){
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(fl.getId(), messages).addToBackStack("stack");;
                ft.commit();
            }
            messages.setContact(p.username);
            messages.setChat(intent.getStringExtra(Intent.EXTRA_TEXT));
            intent = null;
        }
        else{
            if(fl != null ){
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(fl.getId(), messages).addToBackStack("stack");;
                ft.commit();
            }
            messages.setContact(p.username);

        }

    }


    @Override
    public Database getDatabase() {
        return this.db;
    }

    @Override
    public ContactHelper getContactHelper() {
        return this.contactHelper;
    }

    @Override
    public void backToContacts() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(fl.getId(), contacts).addToBackStack("stack");
        ft.commit();
    }

    @Override
    public boolean isLand() {
        return fl == null;
    }


}
