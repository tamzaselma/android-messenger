package android.mbds.tamzaliselma.tp1;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class Login extends AppCompatActivity {

    EditText loginBox;
    EditText passBox;
    Button validBtn;
    Button registerBtn;
    Database database;

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        this.database = Database.getInstance(getApplicationContext());
        loginBox = (EditText)findViewById(R.id.login_box);
        passBox = (EditText)findViewById(R.id.pass_box);
        validBtn = (Button) findViewById(R.id.valide_btn);
        validBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    login();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        registerBtn = (Button) findViewById(R.id.register_btn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    public void login() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("username", loginBox.getText().toString());
        obj.put("password", passBox.getText().toString());
        ApiRest apiRest = new ApiRest(new OkHttpClient());
        apiRest.login(obj.toString(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                validBtn.setBackgroundColor(Color.RED);
                Toast toast = Toast.makeText(getApplicationContext(), "Bad ID", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        JSONObject userInfoJson = new JSONObject(jsonData);
                        database.setCurrentUserInfo(userInfoJson);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        finish();
                    }
                }
            }
        });
    }

    public void register(){
        Intent i = new Intent(this,Register.class);
        i.putExtra("login", loginBox.getText().toString());
        i.putExtra("pass", passBox.getText().toString());
        startActivity(i);
    }
}

