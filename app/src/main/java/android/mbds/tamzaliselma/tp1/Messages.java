package android.mbds.tamzaliselma.tp1;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class Messages extends Fragment {

    String contact;
    String chat;
    TextView contactBox;
    ListView messagesView;
    EditText message;
    Button send;
    Button back;
    ICallable mCallback;
    Context context;
    ArrayList<String> messagesList;

    public Messages(){
        messagesList = new ArrayList<>();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ICallable) {
            this.mCallback = (ICallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.messages, container, false);
        contactBox = view.findViewById(R.id.contact_name);
        message = view.findViewById(R.id.message_box);
        messagesView = view.findViewById(R.id.listView_messages);
        send = (Button) view.findViewById(R.id.send);
        back = (Button) view.findViewById(R.id.back);
        if(this.mCallback.isLand()){
            back.setVisibility(View.GONE);
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter(view.getContext(), android.R.layout.simple_list_item_1, messagesList);
        //getUserMessages();
        messagesView.setAdapter(adapter);
        send.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                String msg = message.getText().toString();
                try {
                    Crypto c = new Crypto();
                    JSONObject obj = new JSONObject();
                    obj.put("message", c.crypte(msg));
                    obj.put("receiver", "admin");
                    obj.put("token", Database.getINSTANCE().userInfo.get("access_token"));
                    ApiRest apiRest = new ApiRest(new OkHttpClient());
                    apiRest.sendMessage(obj, new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(Call call, Response response) {
                        }
                    });

                } catch (JSONException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | NoSuchProviderException | IllegalBlockSizeException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                messagesList.add(msg);
                messagesView.invalidateViews();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.backToContacts();
            }
        });
        return view;
    }

    public void setContact(String contact){
        this.contact = contact;
    }

    public void setChat(String txt){
        this.chat = txt;
    }

    @Override
    public void onResume() {
        super.onResume();
        contactBox.setText(contact);
    }

    private void getUserMessages() {
        ApiRest apiRest = new ApiRest(new OkHttpClient());
        apiRest.getUserMessages(Database.getINSTANCE().userInfo.get("access_token"), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast toast = Toast.makeText(context, "Fail to load new messages", Toast.LENGTH_SHORT);
                toast.show();
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        Database.getINSTANCE().setMessagesServer(new JSONObject(response.body().string()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        if (contact != null) {
            JSONObject messagesServer = (Database.getINSTANCE().getMessagesServer());
            System.out.print("---> "+ messagesServer);
            for (int x = 0; x < messagesServer.length(); x++) {
                try {
                    if (messagesServer.getString("author").compareTo(contact) == 0) {
                        messagesList.add(messagesServer.get("message").toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            messagesView.invalidateViews();

        }
    }


}
