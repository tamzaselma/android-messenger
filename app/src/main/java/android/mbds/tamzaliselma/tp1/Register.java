package android.mbds.tamzaliselma.tp1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class Register extends AppCompatActivity {

    EditText loginBox;
    EditText passBox;
    EditText emailBox;
    Button validBtn;
    public static Database db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        loginBox = (EditText)findViewById(R.id.login_box);
        passBox = (EditText)findViewById(R.id.pass_box);
        validBtn = (Button) findViewById(R.id.valide_btn);

        validBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                try {
                    inscription();
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
        });

        Intent i = getIntent();
        String login = i.getStringExtra("login");
        String pass = i.getStringExtra("pass");
        loginBox.setText(login);
        passBox.setText(pass);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void inscription() throws JSONException, IOException {
        if(!loginBox.getText().toString().isEmpty()  && !passBox.getText().toString().isEmpty()){
            validBtn.setBackgroundColor(Color.GREEN);
            this.db = Database.getINSTANCE();
            User u = new User(loginBox.getText().toString(), passBox.getText().toString());
            JSONObject obj = new JSONObject();
            obj.put("username", u.username);
            obj.put("password", u.password);
            ApiRest apiRest = new ApiRest(new OkHttpClient());
            apiRest.createUser(obj.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response){
                    if (response.isSuccessful()){
                        finish();
                    }
                }
            });
        }
    }
}
