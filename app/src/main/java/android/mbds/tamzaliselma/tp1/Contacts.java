package android.mbds.tamzaliselma.tp1;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;


public class Contacts extends Fragment {
    RecyclerView recyclerView;
    TextAdapter mAdapter;
    ContactHelper contactHelper;
    ICallable mCallback;
    List<User> listeUsers = new ArrayList<User>();
    public static Database database;


    public Contacts(){
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ICallable) {
            this.mCallback = (ICallable) context;
            this.database = mCallback.getDatabase();
            this.listeUsers = this.database.readUser();
            this.mAdapter = new TextAdapter();
            this.mAdapter.setListeUsers(listeUsers);
            this.contactHelper = mCallback.getContactHelper();
            this.contactHelper.onDowngrade(contactHelper.getWritableDatabase(),contactHelper.DATABASE_VERSION, contactHelper.DATABASE_VERSION+1);
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e)
            {
                if(e.getAction()== MotionEvent.ACTION_UP){
                    View child = rv.findChildViewUnder(e.getX(), e.getY());
                    int pos = rv.getChildAdapterPosition(child);
                    mCallback.transferData(listeUsers.get(pos));
                }
                return true;
            }
            @Override
            public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {}
            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean b) {}
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contacts, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view_contact);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(this.mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;

    }
}