package android.mbds.tamzaliselma.tp1;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class TextAdapter extends RecyclerView.Adapter<TextAdapter.MyViewHolder> {
    private List<User> listeUsers;

    public TextAdapter() {
    }

    public void setListeUsers(List<User> listeUsers){
        this.listeUsers = listeUsers;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cellule, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.txtv.setText(listeUsers.get(i).username);
    }


    @Override
    public int getItemCount() {
        return listeUsers.size();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder{
        TextView txtv;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtv = (TextView)itemView.findViewById(R.id.cellule_contact);
        }
    }

}
